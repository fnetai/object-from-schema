# @fnet/object-from-schema Developer Guide

## Overview
The `@fnet/object-from-schema` library is designed for developers who need to generate YAML or JSON formatted objects from a given JSON schema. This library simplifies the process by leveraging user prompts for input values and supports complex schema features such as `oneOf`, `anyOf`, and `allOf`, along with the ability to use default values from reference objects.

## Installation
To install the library, use either npm or yarn:

```bash
npm install @fnet/object-from-schema
```

or

```bash
yarn add @fnet/object-from-schema
```

## Usage
Below is a step-by-step guide on using `@fnet/object-from-schema` to generate a YAML or JSON object based on a provided JSON schema.

### Basic Example
To generate an object from a schema, you can call the main function exported by the library. Here’s a simple use case:

```javascript
import objectFromSchema from '@fnet/object-from-schema';

const schema = {
  type: 'object',
  properties: {
    name: { type: 'string', description: 'Name of the person' },
    age: { type: 'number', description: 'Age of the person' },
  },
  required: ['name']
};

const ref = {
  name: 'John Doe'
};

(async () => {
  const result = await objectFromSchema({ schema, ref, format: 'yaml' });
  console.log(result); // Outputs a YAML string with comments
})();
```

### Using a Reference Object
The function can accept a reference object that provides default values for the schema properties, allowing easy overriding and default value usages:

```javascript
const ref = {
  name: 'Alice'
};

const result = await objectFromSchema({ schema, ref, format: 'json' });
console.log(result); // Outputs a JSON string with default values applied
```

### Output Formats
The library supports different output formats through the `format` option:
- `json`: Returns the result in JSON format.
- `yaml`: Returns the result in YAML format.
- `all`: Returns both formats.

```javascript
const result = await objectFromSchema({ schema, format: 'all' });
console.log(result.json); // JSON format output
console.log(result.yaml); // YAML format output
```

## Examples

### Example with `oneOf`

```javascript
const schemaWithOneOf = {
  type: 'object',
  oneOf: [
    {
      properties: {
        type: { const: 'student' },
        grade: { type: 'number', description: 'Grade of the student' }
      },
    },
    {
      properties: {
        type: { const: 'teacher' },
        subject: { type: 'string', description: 'Subject taught by the teacher' }
      },
    },
  ],
};

const result = await objectFromSchema({ schema: schemaWithOneOf, format: 'yaml' });
console.log(result); // Outputs YAML with the selected option
```

### Example with `anyOf`

```javascript
const schemaWithAnyOf = {
  type: 'object',
  anyOf: [
    { properties: { skill: { type: 'string', description: 'A skill' } } },
    { properties: { hobby: { type: 'string', description: 'A hobby' } } }
  ],
};

const result = await objectFromSchema({ schema: schemaWithAnyOf, format: 'json' });
console.log(result); // Outputs JSON with multiple selected options
```

## Acknowledgement
This library makes use of the `yaml` library to construct YAML documents. Many thanks to the developers of the `yaml` package for their contributions to simplifying YAML manipulations in JavaScript.