# @fnet/object-from-schema

This project provides a utility for generating structured YAML documents from JSON schemas. It is designed to enhance data input processes by leveraging schemas to prompt users for input, allowing for the creation or updating of YAML files that are well-structured and documented with comments. Users can also provide reference objects to pre-fill values, making the data entry process more efficient.

## How It Works

The utility takes a JSON schema and optionally a reference object, which contains default values. Based on the schema, it prompts users to input data for each schema property in an interactive manner, using various input types like text, numbers, selects, and others. It accommodates optional and required fields and provides default values where applicable. Once the data is captured, it converts it into a YAML document, completing the process by ensuring the YAML file is enriched with comments derived from the schema's descriptions.

## Key Features

- **Interactive Prompts**: Uses intuitive prompts based on schema definitions to guide users through data entry.
- **Flexible Input Handling**: Handles different data types and supports schema features like `oneOf` and nested objects.
- **Reference and Defaults**: Allows using pre-existing objects to supply default values, making data entry faster.
- **Multi-Format Support**: Outputs generated data in JSON, YAML, or both formats depending on user preference.
- **Schema-Based Comments**: Automatically includes comments in YAML from schema descriptions for better documentation.

## Conclusion

This utility simplifies the process of generating and managing YAML documents using JSON schemas. It ensures that the data collected is consistent with the schema specifications and makes the input process both flexible and user-friendly. Whether updating existing data or creating new YAML documents, this tool offers a straightforward approach that includes built-in documentation support.