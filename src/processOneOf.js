import fnetPrompt from '@fnet/prompt';
import collectInputsForSchema from "./collectInputsForSchema";
import processSchemaProperty from './processSchemaProperty';
import processTypeInstance from './processTypeInstance';

/**
 * Process `oneOf` in the schema, allowing the user to select a specific option and collect inputs for the selected schema.
 * 
 * @param {Object} node - The schema object containing `oneOf`.
 * @param {string} path - The current path in the schema (for nested objects).
 * @param {Object} refObject - The reference object for defaults (if available).
 * @returns {Promise<Object>} The collected input for the selected option.
 */
export default async function processOneOf(node, path, refObject, { rootSchema, type_selection } = {}) {
  const choices = node.oneOf.map((option, index) => {
    const propertiesHint = Object.keys(option.properties || {}).join(', ');  // Get property names for the option hint
    const description = option.description || propertiesHint || option.type || option.$ref || 'No description available';
    return {
      name: `Option ${index + 1} (${description})`,  // Use description or properties to describe the option
      value: index,
      description: option.description || `Properties: ${propertiesHint}`,
      node: option,
    };
  });

  const { selectedOption } = await fnetPrompt([
    {
      type: 'select',
      name: 'selectedOption',
      message: 'Choose the option that best fits:',
      choices,
    }
  ]);

  // Find the selected choice and validate it using the name or value
  const selectedChoice = choices.find(choice => choice.value === selectedOption || choice.name === selectedOption);

  if (!selectedChoice) {
    throw new Error('Selected option does not match any available options.');
  }

  const selectedNode = selectedChoice.node;
  if (type_selection) {
    return await processTypeInstance({ path, node: selectedNode, rootSchema });
  }
  else {
    const nextRequiredFields = selectedNode.required || [];
    return await collectInputsForSchema(selectedNode, path, nextRequiredFields, refObject, { rootSchema });
  }
}

