import fnetYaml from '@fnet/yaml';

import collectInputsForSchema from "./collectInputsForSchema";
import constructYamlDocument from './constructYamlDocument';

/**
 * Generates a YAML formatted object based on a provided JSON schema, including custom attributes like `subtype` for enhanced input types.
 * If a `ref` object is provided, it will use the values from the `ref` as defaults for the schema properties.
 * The result is returned as a YAML string with comments.
 * 
 * @param {Object} args - The arguments for the function.
 * @param {string|Object} args.schema - The JSON schema to base the user prompts on.
 * @param {string|Object} [args.ref] - Optional. A reference object or file path/URL to use for default values.
 * @param {string} [args.format="json"] - Optional. The format of the output. Can be "json", "yaml", or "all".
 * @returns {Promise<string>} The generated or revised YAML document as a string.
 */
export default async ({ schema, ref, format = "json" }) => {

  // Load the schema
  schema = await loadSchema(schema);

  // Load the ref (pre-existing object) if provided
  let refObject = {};
  if (ref) {
    refObject = await loadRef(ref);
  }

  // Ensure that schema is properly loaded
  if (!schema || typeof schema !== 'object') {
    throw new Error('Failed to load or parse the schema.');
  }

  const requiredFields = schema.required || [];
  const resultObject = await collectInputsForSchema(schema, '', requiredFields, refObject, { rootSchema: schema });

  // Convert the resulting object into a YAML document with comments
  const yamlDoc = constructYamlDocument(resultObject, schema);

  if (format === "json") return yamlDoc.toJSON();
  else if (format === "yaml") return yamlDoc.toString();
  else if (format === "all") return {
    json: yamlDoc.toJSON(),
    yaml: yamlDoc.toString()
  }
  else throw new Error('Invalid format provided');
};

/**
 * Loads a schema or reference object from a file path, URL, or directly from an object.
 * 
 * @param {string|Object} source - The source of the schema or ref object.
 * @returns {Promise<Object>} The loaded and parsed object.
 */
async function loadSchema(source) {
  if (typeof source === 'object') {
    return source;  // Direct object, return as is
  }

  if (typeof source === 'string') {
    try {
      const { parsed } = await fnetYaml({ file: source });
      return parsed;
    } catch (error) {
      console.error(`Error loading schema from ${source}:`, error);
      throw error;
    }
  }

  throw new Error('Unsupported schema format');
}

/**
 * Loads a reference object from a file path, URL, or directly from an object.
 * This is similar to `loadSchema` but for the `ref` object.
 * 
 * @param {string|Object} source - The source of the reference object.
 * @returns {Promise<Object>} The loaded and parsed reference object.
 */
async function loadRef(source) {
  return await loadSchema(source);  // For simplicity, we use the same loading logic as for schemas
}