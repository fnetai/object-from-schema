import fnetPrompt from '@fnet/prompt';

import processAnyOf from "./processAnyOf";
import processOneOf from './processOneOf';
import collectInputsForSchema from "./collectInputsForSchema";
import getPromptType from "./getPromptType";

/**
 * Process `items` for array types in a JSON schema.
 * Supports nested objects, `oneOf`, `anyOf`, `allOf`, and `$ref` within the `items` schema.
 * 
 * @param {Object} itemsSchema - The schema defining the array items.
 * @param {string} path - The current path in the schema.
 * @param {Object} rootSchema - The root schema to resolve $refs.
 * @returns {Promise<Array>} The collected array items.
 */
export default async function processArrayItems(itemsSchema, path, { rootSchema }) {
  const arrayItems = [];
  let addMore = true;
  let index = 0;

  while (addMore) {
    const itemPath = `${path}[${index}]`;

    let processedItem = {};

    // Forward resolution for items under `oneOf`, `anyOf`, `allOf`, or `$ref`
    if (itemsSchema.$ref) {
      // Resolve the $ref from the root schema
      const refPath = itemsSchema.$ref.replace('#/$defs/', '');
      const refSchema = rootSchema.$defs[refPath];
      if (!refSchema) {
        throw new Error(`Reference ${itemsSchema.$ref} not found in $defs.`);
      }
      processedItem = await collectInputsForSchema(refSchema, itemPath, refSchema.required || [], {}, { rootSchema });
    } else if (itemsSchema.oneOf) {
      // If `oneOf` is used, allow the user to select one of the available options
      processedItem = await processOneOf(itemsSchema, itemPath, {}, { rootSchema, type_selection: true });
    } else if (itemsSchema.anyOf) {
      // If `anyOf` is used, allow the user to select multiple of the available options
      processedItem = await processAnyOf(itemsSchema, itemPath, {}, { rootSchema });
    } else if (itemsSchema.allOf) {
      // If `allOf` is used, process all sub-schemas and merge results
      for (const subSchema of itemsSchema.allOf) {
        const collectedSubSchema = await collectInputsForSchema(subSchema, itemPath, subSchema.required || [], {}, { rootSchema });
        Object.assign(processedItem, collectedSubSchema);
      }
    } else {
      // Handle primitive types or nested objects without `$ref`, `oneOf`, `anyOf`, `allOf`
      const promptType = getPromptType(itemsSchema.type, itemsSchema.subtype);
      const answer = await fnetPrompt([
        {
          type: promptType,
          name: itemPath,
          message: `Enter the value for ${itemPath}:`,
        }
      ]);
      processedItem = answer[itemPath];
    }

    // Add the processed item to the array
    arrayItems.push(processedItem);

    // Ask user if they want to add more items to the array
    const { moreItems } = await fnetPrompt([
      {
        type: 'confirm',
        name: 'moreItems',
        message: 'Do you want to add another item to the array?',
        initial: false
      }
    ]);

    addMore = moreItems;
    index++;
  }

  return arrayItems;
}