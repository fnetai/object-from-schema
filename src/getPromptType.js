/**
 * Function to determine the Enquirer prompt type based on JSON Schema's type and subtype.
 *
 * @param {string} type - JSON Schema property 'type'.
 * @param {string} [subtype] - Optional custom 'subtype' for more specific prompt.
 * @returns {string} The Enquirer prompt type.
 */
export default function getPromptType(type, subtype) {
  // If subtype is provided, prioritize it
  if (subtype) {
    switch (subtype) {
      case 'password':
        return 'password'; // Secure input
      case 'autocomplete':
        return 'autocomplete'; // Autocomplete for lists
      case 'multiselect':
        return 'multiselect'; // Allow multiple selections
      case 'select':
        return 'select'; // Single select from list
      // Add more subtypes as needed
      default:
        console.warn(`Unknown subtype: ${subtype}, falling back to default type.`);
        break;
    }
  }

  // Map JSON Schema 'type' to Enquirer prompt types
  switch (type) {
    case 'string':
      return 'input'; // Default string input
    case 'number':
    case 'integer':
      return 'numeral'; // Number input
    case 'boolean':
      return 'confirm'; // Yes/No confirmation
    case 'array':
      return 'multiselect'; // Multiple choice for arrays
    case 'object':
      return 'form'; // Object input, can be a form
    default:
      console.warn(`Unknown JSON Schema type: ${type}, falling back to 'input'.`);
      return 'input'; // Fallback to 'input' for unknown types
  }
}
