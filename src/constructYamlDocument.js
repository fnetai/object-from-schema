import { Document, isScalar, isPair } from 'yaml';

/**
 * Constructs a YAML document from the provided object and schema.
 * Adds comments recursively based on the schema's `description` and `default` values.
 * 
 * @param {Object} obj - The object to convert to a YAML document.
 * @param {Object} schema - The schema to use for adding comments to the YAML.
 * @returns {Document} The constructed YAML document.
 */
export default function constructYamlDocument(obj, schema) {
  const yamlDoc = new Document(obj);

  // Recursive helper function to add comments
  function addComments(node, schemaNode) {
    if (!schemaNode || typeof schemaNode !== 'object') return;

    if (schemaNode.properties) {
      for (const [key, value] of Object.entries(schemaNode.properties)) {
        // Find the pair (key-value) node in the YAML document
        const pair = node.items.find((item) => isPair(item) && item.key.value === key);

        if (pair) {
          // Add description as a comment before the key
          if (value.description) {
            pair.key.commentBefore = value.description;
          }

          // Add default value as a comment to the value node
          if (value.default !== undefined && isScalar(pair.value)) {
            pair.value.comment = `Default: ${value.default}`;
          }

          // Recursively handle nested objects
          if (value.type === 'object' && pair.value) {
            addComments(pair.value, value);
          }
        }
      }
    }
  }

  // Start recursion
  addComments(yamlDoc.contents, schema);

  return yamlDoc;
}
