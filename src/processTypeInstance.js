import fnetPrompt from '@fnet/prompt';

import processAnyOf from "./processAnyOf";
import processOneOf from './processOneOf';
import collectInputsForSchema from "./collectInputsForSchema";
import getPromptType from "./getPromptType";
import processArrayItems from "./processArrayItems";

/**
 * Process a single schema property and collect the corresponding user input.
 * Uses values from `refObject` as defaults if available.
 * Handles `oneOf`, `anyOf`, `allOf`, and `$ref` if present in the value.
 * 
 * @param {string} key - Property key in the schema.
 * @param {Object} value - Schema property object.
 * @param {string} path - Current path in the schema (for nested objects).
 * @param {Array<string>} requiredFields - List of required fields for the current schema.
 * @param {any} existingValue - The value from the `refObject` to be used as default (if available).
 * @param {Object} rootSchema - The root schema to resolve $refs.
 * @returns {Promise<any>} The collected or defaulted user input for the property.
 */
export default async ({ path, node, rootSchema }) => {
  const fullPath = ``;
  const isRequired = false;
  const defaultValue = node.default;
  const hasDefault = defaultValue !== undefined;
  const existingValue = undefined;
  const key = 'temp';

  // If the property has a $ref, resolve it from $defs in the root schema
  if (node.$ref) {
    const refPath = node.$ref.replace('#/$defs/', '');
    node = rootSchema.$defs[refPath];
    if (!node) {
      throw new Error(`Reference ${node.$ref} not found in $defs.`);
    }
  }

  // If existingValue is provided, use it as the initial value
  const initialValue = existingValue !== undefined ? existingValue : (hasDefault ? defaultValue : undefined);

  
  // Handle nested objects
  if (node.type === 'object') {
    const nextRequiredFields = node.required || [];
    return await collectInputsForSchema(node, ``, nextRequiredFields, existingValue || {}, { rootSchema });
  }

  // Handle arrays
  if (node.type === 'array' && node.items) {
    return await processArrayItems(node.items, fullPath, rootSchema);
  }

  // Handle oneOf in value
  if (node.oneOf) {
    return await processOneOf(node, fullPath, existingValue || {}, { rootSchema });
  }

  // Handle anyOf in value
  if (node.anyOf) {
    return await processAnyOf(node, fullPath, existingValue || {}, rootSchema);
  }

  // Handle allOf in value
  if (node.allOf) {
    let combinedResult = {};
    for (const subSchema of node.allOf) {
      const collectedSubSchema = await collectInputsForSchema(subSchema, `${fullPath}.`, subSchema.required || [], existingValue || {}, rootSchema);
      Object.assign(combinedResult, collectedSubSchema);
    }
    return combinedResult;
  }

  const promptType = getPromptType(node.type, node.subtype);

  // Handle primitive types
  const answer = await fnetPrompt([
    {
      type: promptType,
      name: key,
      message: `${path}`,
      initial: initialValue,
      validate: (input) => {
        if (isRequired && (input === null || input === undefined || input === '')) {
          return 'This field is required.';
        }
        if (typeof input === 'string' && isRequired && input.trim() === '') {
          return 'This field is required.';
        }
        return true;
      },
    }
  ]);

  return answer[key] !== '' ? answer[key] : initialValue;
}