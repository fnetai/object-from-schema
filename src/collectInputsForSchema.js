import processAnyOf from "./processAnyOf";
import processOneOf from './processOneOf';
import processSchemaProperty from "./processSchemaProperty";
import processArrayItems from "./processArrayItems";

/**
 * Recursively collects user inputs based on the provided schema.
 * Supports nested objects, arrays, default values, optional fields, oneOf, anyOf, allOf.
 * Uses ref values if provided for default inputs. If the value is undefined, it will return null.
 * 
 * @param {Object} node - JSON Schema object to base the prompts on.
 * @param {string} path - Current path in the schema (for nested objects).
 * @param {Array<string>} requiredFields - List of required fields for the current schema.
 * @param {Object} refObject - The reference object to use for default values (if available).
 * @returns {Promise<Object>} The collected input as an object.
 */
export default async function collectInputsForSchema(node, path = '', requiredFields = [], refObject = {}, { rootSchema } = {}) {
  const obj = { ...refObject };  // Start with the refObject if available

  // Process schema properties
  if (node.properties) {
    for (const [key, value] of Object.entries(node.properties)) {
      obj[key] = await processSchemaProperty(key, value, path, requiredFields, obj[key], { rootSchema });
      if (obj[key] === undefined) {
        obj[key] = null;  // Convert undefined to null
      }
    }
  }


  // Process schema arrays (items support)
  if (node.type === 'array' && node.items) {
    obj[path.trim()] = await processArrayItems(node.items, path, { rootSchema });
  }

  // Process schema oneOf
  if (node.oneOf) {
    const selectedOneOf = await processOneOf(node, path, obj, { rootSchema });
    if (selectedOneOf) {
      Object.assign(obj, selectedOneOf);
    }
  }

  // Process schema anyOf
  if (node.anyOf) {
    const selectedAnyOf = await processAnyOf(node, path, obj, { rootSchema });
    if (selectedAnyOf) {
      Object.assign(obj, selectedAnyOf);
    }
  }

  // Process schema allOf
  if (node.allOf) {
    for (const subSchema of node.allOf) {
      const collectedSubSchema = await collectInputsForSchema(subSchema, path, subSchema.required || [], obj, { rootSchema });
      Object.assign(obj, collectedSubSchema);
    }
  }

  return obj;
}