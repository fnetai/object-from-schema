import fnetPrompt from '@fnet/prompt';

import collectInputsForSchema from "./collectInputsForSchema";

/**
 * Process `anyOf` in the schema, allowing the user to select multiple options and collect inputs for the selected schemas.
 * 
 * @param {Object} schema - The schema object containing `anyOf`.
 * @param {string} path - The current path in the schema (for nested objects).
 * @param {Object} refObject - The reference object for defaults (if available).
 * @returns {Promise<Object>} The collected input for the selected options.
 */
export default async function processAnyOf(schema, path, refObject, { rootSchema } = {}) {
  const choices = schema.anyOf.map((option, index) => {
    const propertiesHint = Object.keys(option.properties || {}).join(', ');  // Get property names for the option hint
    const description = option.description || propertiesHint || 'No description available';
    return {
      name: `Option ${index + 1} (${description})`,  // Use description or properties to describe the option
      value: index,
      description: option.description || `Properties: ${propertiesHint}`,
      schema: option,
    };
  });

  const { selectedOptions } = await fnetPrompt([
    {
      type: 'multiselect',
      name: 'selectedOptions',
      message: 'Choose the options that best fit:',
      choices,
    }
  ]);

  let collectedInputs = {};
  for (const selectedIndex of selectedOptions) {
    const selectedChoice = choices.find(choice => choice.value === selectedIndex || choice.name === selectedIndex);
    if (selectedChoice) {
      const selectedSchema = selectedChoice.schema;
      const nextRequiredFields = selectedSchema.required || [];
      const collectedInput = await collectInputsForSchema(selectedSchema, path, nextRequiredFields, refObject, { rootSchema });
      Object.assign(collectedInputs, collectedInput);
    }
  }

  return collectedInputs;
}